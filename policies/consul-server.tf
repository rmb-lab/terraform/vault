module "consul-server" {
  source = "../policy_module"
  application = "consul-server"
  kubernetes_namespace = "consul"
  custom_vault_policy = <<EOT
path "pki_consul/issue/server" {
  capabilities  = ["create"]
}
EOT
}
