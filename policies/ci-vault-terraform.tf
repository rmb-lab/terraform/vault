module "ci-vault-terraform" {
  source = "../policy_module"
  application = "ci-vault-terraform"
  kubernetes_namespace = "gitlab"
  custom_vault_policy = <<EOT
# Read Consul ACL Token
path "secret/consul-server/acl-token" {
  capabilities  = ["read", "list"]
}

# Mounts
path "sys/mounts" {
  capabilities  = ["read", "list"]
}
path "sys/mounts/*" {
  capabilities  = ["create", "read", "update", "delete"]
}

# Consul
path "consul/roles" {
  capabilities  = ["read", "list"]
}
path "consul/roles/*" {
  capabilities  = ["create", "read", "update", "delete"]
}

# PKI Config CA
path "pki_consul/config/ca" {
  capabilities  = ["create"]
}
path "pki_vms/config/ca" {
  capabilities  = ["create"]
}
path "pki_k8s/config/ca" {
  capabilities  = ["create"]
}

# PKI Config URLS
path "pki_consul/config/urls" {
  capabilities  = ["create", "read"]
}
path "pki_vms/config/urls" {
  capabilities  = ["create", "read"]
}
path "pki_k8s/config/urls" {
  capabilities  = ["create", "read"]
}

# PKI Roles
path "pki_consul/roles/*" {
  capabilities  = ["create", "read", "update", "delete"]
}
path "pki_vms/roles/*" {
  capabilities  = ["create", "read", "update", "delete"]
}
path "pki_k8s/roles/*" {
  capabilities  = ["create", "read", "update", "delete"]
}

# Auth
path "sys/auth" {
  capabilities  = ["read", "list"]
}
path "sys/auth/kubernetes" {
  capabilities  = ["create", "read", "update", "delete"]
}
path "auth/kubernetes/role" {
  capabilities  = ["read", "list"]
}
path "auth/kubernetes/role/*" {
  capabilities  = ["create", "read", "update", "delete"]
}
EOT
}
