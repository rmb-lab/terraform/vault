provider "vault" {
  address = "${var.vault_address}"
  token = "${var.vault_token}"
}

# terraform {
#   backend "consul" {
#     address = "${var.consul_address}"
#     path = "terraform/vault/"
#     access_token = "${var.consul_token}"
#   }
# }
