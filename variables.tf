variable "vault_address" {
  default = "https://vault.kube0-ing.vmw.rmb938.me"
}

variable "consul_address" {
  default = "https://consul.kube0-ing.vmw.rmb938.me"
}

variable "vault_token" {
  type = "string"
}

variable "consul_token" {
  type = "string"
}
