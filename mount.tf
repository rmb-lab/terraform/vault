resource "vault_mount" "pki_consul" {
  path = "pki_consul"
  type = "pki"
  description = "PKI Backend for Consul Certificates"
}

resource "vault_mount" "consul" {
  path = "consul"
  type = "consul"
  description = "Consul Backend for Consul Tokens"
  default_lease_ttl_seconds = 3600
}
