resource "vault_policy" "policy" {
  name = "${var.application}"
  policy = <<EOT
# Common Secrets
path "secret/common/*" {
  capabilities  = ["read", "list"]
}

# Secrets for the application
path "secret/${var.application}/*" {
  capabilities  = ["read", "list"]
}

# Acquire a consul role for the application
path "consul/roles/${var.application}" {
  capabilities = ["read"]
}

# Read CA information
path "pki_vms/cert/ca" {
  capabilities  = ["read"]
}
path "pki_k8s/cert/ca" {
  capabilities  = ["read"]
}
path "pki_consul/cert/ca" {
  capabilities  = ["read"]
}

# Create a cert from PKI
${var.kubernetes_namespace != "" ? "path \"pki_k8s/issue/${var.application}\" {capabilities  = [\"create\"]}" : ""}
${var.sandwich_cloud_project != "" ? "path \"pki_vms/issue/${var.application}\" {capabilities  = [\"create\"]}" : ""}

# Custom Policies
${var.custom_vault_policy}
EOT
}
