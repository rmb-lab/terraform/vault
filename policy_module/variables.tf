variable "application" {
  type = "string"
}

variable "custom_vault_policy" {
  type = "string"
  default = ""
}

variable "kubernetes_namespace" {
  type = "string"
  default = ""
}

variable "kubernetes_service_account" {
  type = "string"
  default = "default"
}

variable "sandwich_cloud_project" {
  type = "string"
  default = ""
}

variable "sandwich_cloud_service_account" {
  type = "string"
  default = "default"
}
