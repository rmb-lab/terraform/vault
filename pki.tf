resource "vault_pki_config_ca" "consul-ca" {
  backend = "${vault_mount.pki_consul.path}"
  cert = "${file("cas/consul.crt")}"
  key = "${file("cas/consul.key")}"
}

resource "vault_pki_config_urls" "consul-ca" {
  backend = "${vault_pki_config_ca.consul-ca.backend}"
  issuing_certificates = [
    "${var.vault_address}/v1/${vault_pki_config_ca.consul-ca.backend}/ca"
  ],
  crl_distribution_points = [
    "${var.vault_address}/v1/${vault_pki_config_ca.consul-ca.backend}/crl"
  ],
}

resource "vault_pki_role" "consul-server" {
  backend = "${vault_pki_config_ca.consul-ca.backend}"
  name = "server"
  allowed_domains = [
    "consul.kube0-ing.vmw.rmb938.me",
    "consul.service.consul",
    "consul.service.*.consul",
  ],
  allow_bare_domains = true
  allow_glob_domains = true
  client_flag = false
  generate_lease = true
  
  organization = ["Home Lab"]
  country = ["USA"]
  province = ["Minnesota"]
  locality = ["Minneapolis"]
}
