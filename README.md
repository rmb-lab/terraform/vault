# Vault Terraform

## Bootstrapping Steps

1. Deploy Vault
    1. Run it with a service account with these permissions https://www.vaultproject.io/docs/auth/kubernetes.html#configuring-kubernetes
1. Initialize Vault
    1. Exec into and run `vault-init.sh`
    1. Save the unseal key and redeploy vault with the key
    1. Wait for vault to come up unsealed
1. Edit `provider.tf` and comment out the backend configuration
1. Run `terraform plan -out=plan.file` and provide a dummy consul token
1. Check the output and make sure everything looks correct
1. Run `terraform apply plan.file`
1. Generate and save a consul acl token `vault write secret/consul-server/acl-token value=$(uuidgen)`
1. Generate and save a consul encrypt key `vault write secret/common/consul/encrypt-key value=$(consul keygen)`
1. Exec into a Vault pod and run `consul-setup.sh` and `kubernetes-setup.sh`
    1. Both scripts will ask for the unseal key to generate vault creds
1. Deploy Consul
1. Push the local state up to consul `consul kv put terraform/vault/terraform.state @terraform.tfstate`
1. Done
